.. _sect_references:

References
##########

.. _references:

.. [Blatter19] Blatter, Daniel, et al. "Bayesian joint inversion of controlled source electromagnetic and magnetotelluric
   data to image freshwater aquifer offshore New Jersey." *Geophysical Journal International* 218.3 (2019): 1822-1837.
   DOI: `10.1093/gji/ggz253 <https://doi.org/10.1093/gji/ggz253>`_
.. [WardHohmann87] Ward, Stanley H., and Gerald W. Hohmann. "Electromagnetic theory for geophysical applications."
   *Electromagnetic Methods in Applied Geophysics: Voume 1, Theory*. Society of Exploration Geophysicists, 1988. 130-311.
   DOI: `10.1190/1.9781560802631.ch4 <https://doi.org/10.1190/1.9781560802631.ch4>`_
.. [Key09] Key, Kerry. "1D inversion of multicomponent, multifrequency marine CSEM data: Methodology and synthetic
   studies for resolving thin resistive layers." *Geophysics* 74.2 (2009): F9-F20.
   DOI: `10.1190/1.3058434 <https://library.seg.org/doi/abs/10.1190/1.3058434>`_.
.. [Sherman15] Sherman, Dallas, Peter Kannberg, and Steven Constable. "Surface towed electromagnetic system for mapping
   of subsea Arctic permafrost." Earth and Planetary Science Letters 460 (2017): 97-104. DOI:
   `10.1016/j.epsl.2016.12.002 <https://doi.org/10.1016/j.epsl.2016.12.002>`_.
.. [Gustafson19] Gustafson, Chloe, Kerry Key, and Rob L. Evans. "Aquifer systems extending far offshore on the US
   Atlantic margin." *Scientific reports* 9.1 (2019): 1-10.
   DOI: `s41598-019-44611-7 <https://www.nature.com/articles/s41598-019-44611-7>`_.
