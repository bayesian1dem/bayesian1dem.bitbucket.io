

<!DOCTYPE html>
<html class="writer-html5" lang="en" >
<head>
  <meta charset="utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <title>Under the hood &mdash; Bayesian1DEM  documentation</title>
  

  
  <link rel="stylesheet" href="_static/css/theme.css" type="text/css" />
  <link rel="stylesheet" href="_static/pygments.css" type="text/css" />

  
  
  
  

  
  <!--[if lt IE 9]>
    <script src="_static/js/html5shiv.min.js"></script>
  <![endif]-->
  
    
      <script type="text/javascript" id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
        <script src="_static/jquery.js"></script>
        <script src="_static/underscore.js"></script>
        <script src="_static/doctools.js"></script>
        <script src="_static/language_data.js"></script>
    
    <script type="text/javascript" src="_static/js/theme.js"></script>

    
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Inputs" href="Inputs.html" />
    <link rel="prev" title="Installation" href="Installation.html" /> 
</head>

<body class="wy-body-for-nav">

   
  <div class="wy-grid-for-nav">
    
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search" >
          

          
            <a href="index.html" class="icon icon-home" alt="Documentation Home"> Bayesian1DEM
          

          
          </a>

          
            
            
          

          
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>

          
        </div>

        
        <div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="main navigation">
          
            
            
              
            
            
              <p class="caption"><span class="caption-text">User Manual</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="Introduction.html">Introduction</a></li>
<li class="toctree-l1"><a class="reference internal" href="Installation.html">Installation</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Under the hood</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#parameterization">Parameterization</a></li>
<li class="toctree-l2"><a class="reference internal" href="#markov-chains">Markov chains</a></li>
<li class="toctree-l2"><a class="reference internal" href="#output-the-model-ensemble">Output: the model ensemble</a></li>
<li class="toctree-l2"><a class="reference internal" href="#flexibility">Flexibility</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="Inputs.html">Inputs</a></li>
<li class="toctree-l1"><a class="reference internal" href="Examples.html">Examples</a></li>
<li class="toctree-l1"><a class="reference internal" href="References.html">References</a></li>
</ul>

            
          
        </div>
        
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap">

      
      <nav class="wy-nav-top" aria-label="top navigation">
        
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="index.html">Bayesian1DEM</a>
        
      </nav>


      <div class="wy-nav-content">
        
        <div class="rst-content">
        
          















<div role="navigation" aria-label="breadcrumbs navigation">

  <ul class="wy-breadcrumbs">
    
      <li><a href="index.html" class="icon icon-home"></a> &raquo;</li>
        
      <li>Under the hood</li>
    
    
      <li class="wy-breadcrumbs-aside">
        
            
            <a href="_sources/Under the hood.rst.txt" rel="nofollow"> View page source</a>
          
        
      </li>
    
  </ul>

  
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
            
  <div class="section" id="under-the-hood">
<h1>Under the hood<a class="headerlink" href="#under-the-hood" title="Permalink to this headline">¶</a></h1>
<p>What is the code doing while it’s running? Let’s take a peek under the hood.</p>
<div class="section" id="parameterization">
<h2>Parameterization<a class="headerlink" href="#parameterization" title="Permalink to this headline">¶</a></h2>
<p>Models in <strong>Bayesian1DEM</strong>, are parameterized as a stack of layers, each of which is assigned a constant value of
bulk resistivity. The layers themselves are defined as sequence of layer interface depths. Due to the diffusive
nature of most EM methods, as well as the wide range of bulk resistivities in Earth materials, it is often best to
invert for the base-10 logarithm of both the layer resistivities and the layer interface depths. <strong>Bayesian1DEM</strong>
omits the interface at z=0 and assumes that the layer underneath the final interface is a halfspace.</p>
<p>Following <a class="reference internal" href="References.html#blatter19" id="id1"><span>[Blatter19]</span></a>, <strong>Bayesian1DEM</strong> samples the subsurface using two model regions, each with their own inversion
parameters. This was designed for inversion of marine CSEM data where the properties of the water column are vastly
different than the properties of the subsurface (yet getting the water column resistivity right is crucial). Thus, by
default model region 1 is above model region 2 in depth and represents the water column, while model region 2 represents
the subsurface. <strong>Bayesian1DEM</strong> samples these two regions separately (the relative frequency determined by an input
parameter), but the forward modeling of course uses the full, composite model.</p>
</div>
<div class="section" id="markov-chains">
<h2>Markov chains<a class="headerlink" href="#markov-chains" title="Permalink to this headline">¶</a></h2>
<p><strong>Bayesian1DEM</strong> uses Markov chains, which are sequences of models that grow iteratively by adding models to the end of the
chain. At each step in the algorithm, <strong>Bayesian1DEM</strong> generates a new model (called the “proposal”) by modifying
the current model in one of four ways:</p>
<ul class="simple">
<li><p>Adding a new layer interface at a random location</p></li>
<li><p>Deleting a current layer interface (chosen at random)</p></li>
<li><p>Moving a randomly chosen existing layer interface to a new location (the new location is chosen by a Gaussian
distribution whose mean is set to the interface’s current location)</p></li>
<li><p>Changing the resistivity of each layer using a Gaussian distribution with mean equal to each layer’s
current resistivity value</p></li>
</ul>
<p><strong>Bayesian1DEM</strong> then accepts or rejects the proposal model on the basis of how well it fits the data and
our prior assumptions (its posterior probability) compared to the current model, using the standard Metropolis-Hastings
accept-reject criterion. If the proposal is accepted, it becomes the next model in the chain. If it is rejected,
the current model is copied and becomes the next model in the chain as well. The algorithm then repeats, generating
a new proposal model from the new current model.</p>
</div>
<div class="section" id="output-the-model-ensemble">
<h2>Output: the model ensemble<a class="headerlink" href="#output-the-model-ensemble" title="Permalink to this headline">¶</a></h2>
<p>After the chain is sufficiently long such that the statistics of the model parameters become stationary (they aren’t
changing with time) and the initial stage as the chain finds models that fit the data (the “burn-in”) is removed,
the models in the Markov chain represent samples drawn from the Bayesian postrior distribution. In other words, the
statistics of the model parameters across all the models in the chain (the “model ensemble”) represent the Bayesian
posterior uncertainty. Thus, once <strong>Bayesian1DEM</strong> has finished running, the output is a collection of models (saved
in the output .mat files under the variable <em>s_ll</em>) that can be mined for all sorts of statistical information: mean,
median, variance, interquartile range, 90% credible interval, etc.</p>
</div>
<div class="section" id="flexibility">
<h2>Flexibility<a class="headerlink" href="#flexibility" title="Permalink to this headline">¶</a></h2>
<p><strong>Bayesian1DEM</strong> is both physical property and data-type invariant. The core of the algorithm knows only that the
“model” is a stack of layers defined by a series of layer interfaces with one or more constant properties ascribed
to each layer. So long as you have a forward modeling code that you can call to generate modeled data given the current
model as defined by <strong>Bayesian1DEM</strong>, you are good to go. This means you can rather easily modify <strong>Bayesian1DEM</strong> to
invert whatever data you want, so long as a trans-dimensional, piecewise-constant stack of layers is an appropriate
model parameterization.</p>
</div>
</div>


           </div>
           
          </div>
          <footer>
  
    <div class="rst-footer-buttons" role="navigation" aria-label="footer navigation">
      
        <a href="Inputs.html" class="btn btn-neutral float-right" title="Inputs" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right"></span></a>
      
      
        <a href="Installation.html" class="btn btn-neutral float-left" title="Installation" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left"></span> Previous</a>
      
    </div>
  

  <hr/>

  <div role="contentinfo">
    <p>
        
        &copy; Copyright 2020, Daniel Blatter, Anandaroop Ray

    </p>
  </div>
    
    
    
    Built with <a href="http://sphinx-doc.org/">Sphinx</a> using a
    
    <a href="https://github.com/rtfd/sphinx_rtd_theme">theme</a>
    
    provided by <a href="https://readthedocs.org">Read the Docs</a>. 

</footer>

        </div>
      </div>

    </section>

  </div>
  

  <script type="text/javascript">
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script>

  
  
    
   

</body>
</html>